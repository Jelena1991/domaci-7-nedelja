
<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = $dateErr = "";
$name = $email = $gender = $comment = $website = $date=  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } 
  else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
    else {
      $name1 = $_POST['name'];
      // check if name contain at least two characters
      if (strlen($name1) <2) {    
        $nameErr = "Name must contain at least two characters";
      }
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } 
  else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; }
    else {
      include 'checkEmailAdress.php';
      if (isset($_POST['email'])) {
        $email1 = $_POST['email'];
        // check if e-mail address is gmail.com
        if (is_email($email1) == false) {
          $emailErr = "It's not gmail.com";
        } 
        else {
          $user = strstr($email1, '@', true);
          // check if username contain at least six characters
          if (strlen($user) <6) {    
            $emailErr = "Username must contain at least six characters";
          }
        }
      }
    }
  }

  if (empty($_POST["date"])) {
    $dateErr = "Date is required";
  } 
  else {
    $date = test_input($_POST["date"]);
  }
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL"; 
    }
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
  
  if (!empty($nameErr) or !empty($emailErr) or !empty($websiteErr) or !empty($commentErr) or !empty($genderErr)or !empty($dateErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&website=" . urlencode($_POST["website"]);
    $params .= "&comment=" . urlencode($_POST["comment"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);
    $params .= "&date=" . urlencode($_POST["date"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&websiteErr=" . urlencode($websiteErr);
    $params .= "&commentErr=" . urlencode($commentErr);
    $params .= "&genderErr=" . urlencode($genderErr);
    $params .= "&dateErr=" . urlencode($dateErr);
    
    header("Location: index2.php?" . $params);
  } else {
    echo "<h2>Your Input:</h2>";
    echo "Name: " . $_POST['name'];
    echo "<br>";
    
    echo "Email: " . $_POST['email'];
    echo "<br>";
    
    echo "Website: " . $_POST['website'];
    echo "<br>";
    
    $date1 = date('l', strtotime($_POST['date']));
    echo "Date o birth: " . $date1. " ".$_POST['date'];  
    echo "<br>";

    echo "Comment: " . $_POST['comment'];
    echo "<br>";
    
    echo "Gender: " . $_POST['gender'];  
    echo "<br>";
    echo "<br>";

    echo "<a href=\"index2.php\">Return to form</a>";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>