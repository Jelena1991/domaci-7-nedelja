# Program prekvalifikacija - 7. nedelja

### Domaći zadatak:
- Kreirati stranicu koja ima obrazac za registraciju (osnovni podaci, datum rođenja i još neka polja), podatke slati na register.php stranicu POST metodom.
- Stranica register.php proverava da li su sva polja ispunjena ako neko nije preusmerava korisnika na početnu stranicu i ispisuje poruku o grešci.
- Ukoliko je sve u redu stranica register ispisuje sve unete podatke a za uneti datum ispisuje ime dana na engleskom jeziku za taj datum.
- Unete podatke očistiti od suvišnih praznih mesta primenom odgovarajućih funkcija.
- Napraviti dodatnu proveru za dužinu imena i prezimena i email adrese. Minimalna dužina imena i prezimena je 2 znaka, a email adrese 6 znakova.
- Napraviti dodatnu proveru email adrese, pri čemu je kriterijum da email adresa pripada domenu \@gmail.com. U suprotnom ispisati odgovarajuću grešku. Za potrebe provere email adrese napraviti funkciju checkEmailAdress koja kao parametar prihvata email adresu, a kao rezultat vraća vrednost logičkog tipa - true ili false.
 